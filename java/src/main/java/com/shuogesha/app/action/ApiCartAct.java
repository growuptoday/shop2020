package com.shuogesha.app.action;

import static com.shuogesha.platform.web.mongo.SimplePage.cpn;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shuogesha.app.version.AccessTokenRequired;
import com.shuogesha.app.web.util.ApiUtils;
import com.shuogesha.cms.entity.Cart;
import com.shuogesha.cms.entity.Product;
import com.shuogesha.cms.entity.ProductAttr;
import com.shuogesha.cms.service.CartService;
import com.shuogesha.cms.service.ProductAttrService;
import com.shuogesha.cms.service.ProductService;
import com.shuogesha.common.util.JsonResult;
import com.shuogesha.common.util.ResultCode;
import com.shuogesha.platform.web.mongo.Pagination;
/**
 * 购物车
 * @author zhaohaiyuan
 *
 */
@Controller
@RequestMapping("/app/")
public class ApiCartAct {
	private static Logger log = LoggerFactory.getLogger(ApiCartAct.class);
	/**
	 * 商品加入购物车
	 * @param bean
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "save_cart", method = RequestMethod.POST)
 	@AccessTokenRequired
	public @ResponseBody Object save_cart(@RequestBody Cart bean,HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		bean.setUserId(ApiUtils.getUnifiedUserId(request));
		bean.setRefer(Product.class.getSimpleName()); 
		if(bean.getReferid()==null||bean.getReferid()<=0||bean.getNum()<=0) {
			return new JsonResult(ResultCode.FAIL, "参数错误",null);
		}
		Product product=productService.findById(bean.getReferid());
		if(product==null) {
			return new JsonResult(ResultCode.FAIL, "商品不存在",null);
		}
		BigDecimal price=product.getPrice();
		if(bean.getAttrId()!=null&&bean.getAttrId()>0) {
			ProductAttr productAttr=productAttrService.findById(bean.getAttrId());
			if(productAttr==null) {
				return new JsonResult(ResultCode.FAIL, "商品已经下架",null);
			}
			if(productAttr.getPrice()!=null&&productAttr.getPrice().compareTo(BigDecimal.ZERO)>0) {
				price=productAttr.getPrice();
			}
		} 
		//重新设置
		bean.setPrice(price);
		bean.setImg(product.getImg());
		bean.setName(product.getName());
		Cart cart=cartService.findByRefer(bean);
		if(cart!=null) {
			cart.setNum(cart.getNum()+bean.getNum());
			cartService.update(cart);
			return new JsonResult(ResultCode.SUCCESS, cart);
		}else {
			cartService.save(bean);
		} 
		return new JsonResult(ResultCode.SUCCESS, bean);
	}
	/**
	 * 根据购物车的id修改数量
	 * @param bean
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "update_cart", method = RequestMethod.POST)
 	@AccessTokenRequired
	public @ResponseBody Object update_cart(@RequestBody Cart bean,HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if(bean.getId()==null||bean.getId()<=0||bean.getNum()<=0) {
			return new JsonResult(ResultCode.FAIL, "参数错误",null);
		}
		bean.setUserId(ApiUtils.getUnifiedUserId(request));
		Cart cart=cartService.findById(bean.getId());
		if(cart!=null) {
			cart.setNum(bean.getNum());
			cartService.update(cart);
			return new JsonResult(ResultCode.SUCCESS, cart);
		}
 		return new JsonResult(ResultCode.SUCCESS, bean);
	}
	/**
	 * 删除单个购物车的商品
	 * @param id
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "delete_cart")
 	@AccessTokenRequired
	public @ResponseBody Object delete_cart(Long id,HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		cartService.removeById(id);
		return new JsonResult(ResultCode.SUCCESS, "删除成功",true);
	}
	/**
	 * 清空购物车
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "delete_all_cart")
 	@AccessTokenRequired
	public @ResponseBody Object delete_all_cart(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		cartService.removeByUserId(ApiUtils.getUnifiedUserId(request));
		return new JsonResult(ResultCode.SUCCESS, "删除成功",true);
	}
	 
	/**
	 * 我的购物车
	 * @param pageNo
	 * @param pageSize
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "get_cart_list") 
	@AccessTokenRequired
 	public @ResponseBody Object get_cart_list(Integer pageNo, Integer pageSize, 
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) throws IOException { 
		Pagination pagination = cartService.getPage(null,ApiUtils.getUnifiedUserId(request),cpn(pageNo),pageSize);
		return new JsonResult(ResultCode.SUCCESS,pagination);
	}
	
	@Autowired
	private CartService cartService;
	@Autowired
	private ProductService productService;
	@Autowired
	private ProductAttrService productAttrService;
}
